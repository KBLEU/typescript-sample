FROM node:19-alpine3.15 as builder
LABEL stage=builder
WORKDIR /app
COPY src/ src/
COPY tsconfig.json .
RUN npm i -g typescript
RUN tsc

FROM node:19-alpine3.15
WORKDIR /usr/app
COPY --from=builder /app/build build/
ENTRYPOINT [ "node" ]
CMD [ "build/index" ]