import express from "express";
import path from "path";
import dotenv from "dotenv";
// import { exec } from "child_process"

// Identification de l'environnement
const stage = process.env.NODE_ENV || "developpement";

// Sélection du fichier d'environnement
const file_path = path.resolve(process.cwd(), stage == "developpement" ? ".env": ".env.production");
dotenv.config({
    path: file_path
});

const app = express();

console.log("process_env ", process.env.API_URL);

app.get("/index", (req, res) => {
    res.send("ok");
});

app.listen(3500, () => console.log("app is running..."))

console.log("stage ", stage)